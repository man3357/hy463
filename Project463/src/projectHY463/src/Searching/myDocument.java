package Searching;

import java.util.TreeMap;

public class myDocument {
	private int pmcid;
	private double norm;
	private String filepath;
	private TreeMap<String,Double> docWordsW;
	private double cosSim;
	
	myDocument(int pmcid, String filepath, double norm, TreeMap<String,Double> wi){
		this.pmcid = pmcid;
		this.filepath = filepath;
		this.norm = norm;
		this.docWordsW = wi;
	}
	myDocument(String pmcid, String filepath, String norm, String wi){
		this.pmcid = Integer.parseInt(pmcid);
		this.filepath = filepath;
		this.norm = Double.parseDouble(norm);
		String[] tmp = wi.replace(" ", "").replace("{", "").replace("}", "").split(",");
		this.docWordsW = new TreeMap<String,Double>();
		for(String s : tmp) {
			this.docWordsW.put(s.split("=")[0], Double.parseDouble(s.split("=")[1]));
		}
	}
	
	public double getCosSim() {return this.cosSim;}
	public int getPmcid() {return this.pmcid;}
	public String getFilePath() {return this.filepath;}
	
	public void cosSim(myQuery query) {
		double up = 0;
		double IqI = 0;
		double IdI = 0;
		for(String s : this.docWordsW.keySet()) {
			up += this.docWordsW.get(s) * ( (query.getQueryWords().containsKey(s)) ? query.getWordW(s) : 0 );
//			System.out.println(queryWords.get(s) +"|"+wi.get(s));
			IqI += Math.pow( ( (query.getQueryWords().containsKey(s)) ? query.getWordW(s) : 0 ) ,2);
			IdI += Math.pow( this.docWordsW.get(s) , 2);
		}
		double down = Math.sqrt(IqI * IdI);
		this.cosSim = up / down;
	}
}
