package Searching;

public class myWord {
	private String name;
	private int df;
	private long posInPosting;
	
	myWord(String name, String df, String posInPosting){
		this.name = name;
		this.df = Integer.parseInt(df);
		this.posInPosting = Long.parseLong(posInPosting);
	}

	String getName(){return this.name;}
	int getDf(){return this.df;}
	long getPosInPosting(){return this.posInPosting;}
}
