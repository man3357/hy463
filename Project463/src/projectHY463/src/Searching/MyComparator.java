package Searching;

import java.util.Comparator;

class MyComparatorString implements Comparator<String>{
    public int compare(String o1,String o2){
        return o1.compareTo(o2);
    }
}
class MyComparatorInt implements Comparator<String>{
	public int compare(String o1, String o2) {
		int i1 = Integer.parseInt(o1);
		int i2 = Integer.parseInt(o2);
		return i1-i2;
	}
}
class MyComparatorDouble implements Comparator<Double>{
    public int compare(Double o1,Double o2){
        return o1.compareTo(o2);
    }
}
class MyComparatorMyDocument implements Comparator<myDocument>{
    public int compare(myDocument o1,myDocument o2){
    	if(o1.getCosSim() > o2.getCosSim()){
    			return -1;}
    	else if (o1.getCosSim() < o2.getCosSim()){
    			return 1;}
    	else { return 0;}
    }
}
