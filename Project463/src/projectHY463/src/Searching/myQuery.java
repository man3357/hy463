package Searching;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.TreeMap;

import mitos.stemmer.Stemmer;

public class myQuery {
	private HashMap<String, Integer> queryWords;
	private double maxFreq;
	private TreeMap<String, Double> wq;
	
	private static HashMap<String, String> stopwords = StopWords.makeStopWordList();

	myQuery(String query, String type){
		this.maxFreq = 0;
		parseQuery(query);
//		if(type != null) {
//			if(queryWords.get(type) == null){
//				queryWords.put(type,10);
//			}else {
//				queryWords.put(type,queryWords.get(type)+10);
//			}
//		}
		filterQueryWords();
		this.wq();
	}
	
	private void filterQueryWords() {
		for(String w : this.queryWords.keySet()) {
			if(Search.vocabulary.get(w) == null) {
				this.queryWords.remove(w);
			}
		}
	}

	public TreeMap<String, Double> getQueryWords(){return this.wq;}
	public double getWordW(String w) {return this.wq.get(w);}
	
	private void parseQuery(String input) {
		String delimiter = "\b\t\n\r\f\0.,;:'!?[]{}()/\\\'\"*&^%$#@_-+=~<>| ";
		StringTokenizer tokenizer = new StringTokenizer(input, delimiter);
		this.queryWords = new HashMap<String, Integer>();
		while(tokenizer.hasMoreTokens() ) {
			String currentToken = tokenizer.nextToken();
			if(currentToken.equals(null)) continue;
			currentToken = currentToken.toLowerCase();
			if( "1".equals(stopwords.get(currentToken))) {
			}else {
				Stemmer.Initialize();
				currentToken = Stemmer.Stem(currentToken);
				if(this.queryWords.get(currentToken) == null){
					this.queryWords.put(currentToken,1);
				}else {
					this.queryWords.put(currentToken, this.queryWords.get(currentToken)+1);
				}
				if(this.maxFreq < this.queryWords.get(currentToken)) {
					this.maxFreq = this.queryWords.get(currentToken);
				}
			}
		}
	}
	
	private double tf(String s) {
		double mul = 1;
		if(Search.label.equals("title")){
			mul = 3.5;
		}else if(Search.label.equals("authors")) {
			mul = 3;
		}else if(Search.label.equals("categories")) {
			mul = 2.6;
		}else if(Search.label.equals("abstr")) {
			mul = 2.2;
		}else if(Search.label.equals("journal")) {
			mul = 1.8;
		}else if(Search.label.equals("publisher")) {
			mul = 1.4;
		}
		return mul*((double)(this.queryWords.get(s))/this.maxFreq);
	}
	
	private double logb( double a, double b ){return Math.log(a) / Math.log(b);}
	private double log2( double a ){return logb(a,2);}
	
	private double wi(String i) {
		double idf, tf;
		tf = tf(i);
		idf = log2(Search.N / (double) Search.vocabulary.get(i).getDf());
		return tf * idf;
	}
	
	public void wq() {
		TreeMap<String, Double> wq = new TreeMap<String, Double>();
		for(String s : this.queryWords.keySet()) {
			wq.put(s, wi(s));
		}
		this.wq = wq;
	}
	
	public double norm() {
		double sum = 0;
		for(String d : this.wq.keySet()) {
			sum += this.wq.get(d);
		}
		return Math.sqrt(sum);
	}
	
	public ArrayList<myDocument> sort(ArrayList<myDocument> docs){
		for(myDocument d : docs) {
			d.cosSim(this);
		}
		System.out.println("Calculated cosSim");
		docs.sort(new MyComparatorMyDocument());
		return docs;
	}
}
