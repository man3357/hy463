package Searching;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeMap;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Search {
	protected static int N;
	protected static TreeMap<String,myWord> vocabulary = new TreeMap<String,myWord>();
	protected static String label = "body";
	
	private static void loadVocabulary() {
		try {
			JSONParser strParser = new JSONParser();
			File f = new File("CollectionIndex\\Final\\Vocabulary.txt");
			Scanner sc = new Scanner(f);
			while(sc.hasNextLine()) {
				JSONObject json = (JSONObject) strParser.parse(sc.nextLine());
				vocabulary.put(json.get("word").toString(),new myWord(json.get("word").toString(),json.get("df").toString(),json.get("posInPosting").toString()));
			}
			sc.close();
		} catch (FileNotFoundException | ParseException e) {
			e.printStackTrace();
		}
	}
	
	private static ArrayList<myDocument> findDocuments(ArrayList<Long> positions) {
		ArrayList<myDocument> docs = new ArrayList<myDocument>();
		JSONParser strParser = new JSONParser();
		try {
			RandomAccessFile f = new RandomAccessFile("CollectionIndex\\Final\\Documents.txt","rw");
			for(long pos : positions) {
				f.seek(pos);
				JSONObject j = (JSONObject) strParser.parse(f.readLine());
				docs.add(new myDocument(j.get("pmcid").toString(), j.get("filepath").toString(), j.get("norm").toString(), j.get("wij").toString()));
			}
			f.close();
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		return docs;
	}
	
	private static ArrayList<myDocument> search(myQuery query) {
		ArrayList<Long> positions = new ArrayList<Long>();
		JSONParser strParser = new JSONParser();
		RandomAccessFile f;
		try {
			f = new RandomAccessFile("CollectionIndex\\Final\\Posting.txt","rw");
			for(String word : query.getQueryWords().keySet()) {
				myWord w = vocabulary.get(word);
					f.seek(w.getPosInPosting());
					f.readLine();
					for(int i=0; i<w.getDf(); i++) {
						JSONObject j = (JSONObject) strParser.parse(f.readLine());
						if(positions.contains(Long.parseLong(j.get("doc").toString())) ) continue;
						positions.add(Long.parseLong(j.get("doc").toString()));
					}
			}
			f.close();
		} catch (IOException | ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return findDocuments(positions);
	}

	private static String timer(long startTime) {
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		double sec = totalTime/1000;
		int min = (int)sec/60;
		sec = sec%60;
		return min+"minutes and "+(int)sec+"seconds.";
	}
	
	public static ArrayList<myDocument> run(String input, String type) throws ParseException, IOException {
		long startTime = System.currentTimeMillis();
		JSONParser strParser = new JSONParser();
		RandomAccessFile f = new RandomAccessFile("CollectionIndex\\Final\\Documents.txt","rw");
		JSONObject j = (JSONObject) strParser.parse(f.readLine());
		N = Integer.parseInt(j.get("totalFiles").toString());
        loadVocabulary();
        input = input.toLowerCase();
        myQuery query = new myQuery(input, type);
		System.out.println("Made query");
        ArrayList<myDocument> docs = search(query);
		System.out.println("Find documents");
        docs = query.sort(docs);
		System.out.println("Documents sorted");
		System.out.println("Total time: "+timer(startTime));
		return docs;
	}
}
