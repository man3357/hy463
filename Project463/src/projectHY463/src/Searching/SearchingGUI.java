package Searching;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import org.json.simple.parser.ParseException;

import gr.uoc.csd.hy463.NXMLFileReader;

public class SearchingGUI extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel label;
	private JTextField tf;
	private JButton button;
	private ButtonGroup group;
	private JRadioButton diagnosis;
	private JRadioButton test;
	private JRadioButton treatment;
	
	private JPanel searchPanel;
	private JPanel answerPanel;
	private JScrollPane myJScrollPane;
	
	private JFrame docFrame;
	
	SearchingGUI(){
		getContentPane().setBackground(Color.LIGHT_GRAY);
		BorderLayout frameLayout = new BorderLayout();
		setLayout(frameLayout);
        JFrame.setDefaultLookAndFeelDecorated(true);
		this.searchPanel = new JPanel();
		this.searchPanel.setOpaque(false);
		GroupLayout layout = new GroupLayout(this.searchPanel);
		this.searchPanel.setLayout(layout);
		layout.setAutoCreateGaps(true);
		layout.setAutoCreateContainerGaps(true);
		
		this.label = new JLabel("What are you looking for?");
		this.tf = new JTextField(10);
		this.group  = new ButtonGroup();
		this.diagnosis = new JRadioButton("Diagnosis");
		this.diagnosis.setOpaque(false);
		this.diagnosis.setActionCommand("diagnosis");
		this.test = new JRadioButton("Testing");
		this.test.setOpaque(false);
		this.test.setActionCommand("test");
		this.treatment = new JRadioButton("Treatment");
		this.treatment.setOpaque(false);
		this.treatment.setActionCommand("treatment");
		this.group.add(this.diagnosis);
		this.group.add(this.test);
		this.group.add(this.treatment);

		JPanel radioPanel = new JPanel(new GridLayout(1,3));
		radioPanel.setOpaque(false);
		radioPanel.add(this.diagnosis);
		radioPanel.add(this.test);
		radioPanel.add(this.treatment);
		radioPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "About?"));

		this.button = new JButton("Search");
		searchEvent e = new searchEvent();
		button.addActionListener(e);

		layout.setHorizontalGroup(
				   layout.createSequentialGroup()
				      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				           .addComponent(this.label)
				           .addComponent(this.tf)
				           .addComponent(radioPanel))
				      .addComponent(this.button)
				);
		layout.setVerticalGroup(
				   layout.createSequentialGroup()
				   	  .addComponent(this.label)
				      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
					       .addComponent(this.tf)
				           .addComponent(this.button))
				      .addComponent(radioPanel)
				);
		
		add(this.searchPanel, BorderLayout.NORTH);
		this.answerPanel = new JPanel(new GridLayout(0,1));
        this.myJScrollPane = new JScrollPane(this.answerPanel,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		add(this.myJScrollPane, BorderLayout.CENTER);
	}
	
	private void DisplayAnswer(ArrayList<myDocument> docs) {
		getContentPane().remove(this.myJScrollPane);
		this.answerPanel = new JPanel(new GridLayout(0,1));
		this.answerPanel.setOpaque(false);
        for(int i=0; i<docs.size() && i<5; i++) {
    		JPanel docPanel = new JPanel();
    		docPanel.setOpaque(false);
    		GroupLayout layout = new GroupLayout(docPanel);
    		docPanel.setLayout(layout);
//    		layout.setAutoCreateGaps(true);
    		layout.setAutoCreateContainerGaps(true);
        	JLabel path = new JLabel(docs.get(i).getFilePath());
        	JButton open = new JButton("OPEN");
        	open.setActionCommand(path.getText());
        	openFileEvent e = new openFileEvent();
        	open.addActionListener(e);
        	JLabel cosSim = new JLabel(docs.get(i).getCosSim()+"");
        	
        	layout.setHorizontalGroup(
 				   layout.createSequentialGroup()
 				      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
 				           .addComponent(path)
 				           .addComponent(cosSim))
 				      .addComponent(open)
 				);
        	layout.setVerticalGroup(
 				   layout.createSequentialGroup()
 				   	  .addComponent(path)
 				      .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
 					       .addComponent(cosSim)
 				           .addComponent(open))
 				);
        	
        	this.answerPanel.add(docPanel);
        }
        
        this.answerPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Results"));
        this.myJScrollPane = new JScrollPane(this.answerPanel,
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        add(this.myJScrollPane, BorderLayout.CENTER);
		setVisible(true);
		
	}
	
	
	
	public class searchEvent implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String input = tf.getText();
			String type = null;
			if(group.getSelection() != null) {
				type = group.getSelection().getActionCommand();
			}
			if(!input.equals("")) {
				try {
					DisplayAnswer(Search.run(input, type));
				} catch (ParseException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}
	
	public class openFileLocationEvent implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			File f = new File(e.getActionCommand());
			try {
				Desktop.getDesktop().open(f);
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
		}
	}
	
	public class openFileEvent implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String path = e.getActionCommand();
			File f = new File(path);
			String txt = null;
			try {
				txt = fileToString(f);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			StringTokenizer tokenizer = new StringTokenizer(path, "\\");
			String fileName = "";
	        while (tokenizer.hasMoreTokens()) {
	        	fileName = tokenizer.nextToken();
	            //System.out.println(tokenizer.nextToken());
	        }
			docFrame = new JFrame(fileName);
			docFrame.setSize(300, 300);
			Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		    int x = (int) ((dimension.getWidth() - docFrame.getWidth()) / 2);
		    int y = (int) ((dimension.getHeight() - docFrame.getHeight()) / 2);
		    docFrame.setLocation(x-300, 0);
			JPanel docPanel = new JPanel();
			JLabel docTxt = new JLabel(txt);
			JButton openLocation = new JButton("OPEN");
			openFileLocationEvent event = new openFileLocationEvent();
			openLocation.setActionCommand(e.getActionCommand());
			openLocation.addActionListener(event);
			docPanel.add(docTxt);
			docPanel.add(openLocation);
			docFrame.add(new JScrollPane(docPanel,
	                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
	                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
			docFrame.setVisible(true);
		}
		
		private String fileToString(File file) throws UnsupportedEncodingException, IOException {
			NXMLFileReader xmlFile = new NXMLFileReader(file);
			String txt = "<html>";
			byte ptext[];
//			String pmcid = xmlFile.getPMCID();
//			ptext = pmcid.getBytes();
//			txt += "PMCID: ";
//			txt += new String(ptext, "UTF-8");
//			
//			String title = xmlFile.getTitle();
//			ptext = title.getBytes();
//			txt += "\nTitle: ";
//			txt += new String(ptext, "UTF-8");
//			
			String abstr = xmlFile.getAbstr();
			ptext = abstr.getBytes();
			txt += new String(ptext, "UTF-8");
//			
//			String body = xmlFile.getBody();
//			ptext = body.getBytes();
//			txt += "\nBody: ";
//			txt += new String(ptext, "UTF-8");
//			
//			String journal = xmlFile.getJournal();
//			ptext = journal.getBytes();
//			txt += "\nJournal: ";
//			txt += new String(ptext, "UTF-8");
//			
//			String publisher = xmlFile.getPublisher();
//			ptext = publisher.getBytes();
//			txt += "\nPublicher: ";
//			txt += new String(ptext, "UTF-8");
//			
//			ArrayList<String> authors0 = xmlFile.getAuthors();
//			txt += "\nAuthors: ";
//			for(String s: authors0) {
//				ptext = s.getBytes();
//				txt += (new String(ptext, "UTF-8")) + " ";
//			}
//			HashSet<String> categories0 = xmlFile.getCategories();
//			txt += "\nCategories: ";
//			for(String s: categories0) {
//				ptext = s.getBytes();
//				txt += (new String(ptext, "UTF-8")) + " ";
//			}
			return makeLines(txt+"</html>");
		}
		
		private String makeLines(String txt) {
			int m = 0;
			for(int i=0; i<txt.length(); i++) {
				if(txt.charAt(i) == ' ') {
					m++;
				}else if(txt.charAt(i) == '\n') {
					txt = txt.replace(txt.substring(0, i), txt.substring(0, i) + "<br>\t\t");
					m = 0;
				}
				if(m == 8) {
					txt = txt.replace(txt.substring(0, i), txt.substring(0, i) + "<br>\t\t");
					m = 0;
				}
			}
			return txt;
		}
	}
	
	public static void main(String[] args) {
		SearchingGUI gui = new SearchingGUI();
		gui.setDefaultCloseOperation(EXIT_ON_CLOSE);
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((dimension.getWidth() - gui.getWidth()) / 2);
	    int y = (int) ((dimension.getHeight() - gui.getHeight()) / 2);
	    gui.setLocation(x-300, 0);
		gui.setSize(600,500);
		gui.setTitle("Search Machine");
		gui.setVisible(true);
	}
}
