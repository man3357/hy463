package Indexing.tester;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.SortedMap;
import java.util.TreeMap;
import Indexing.PartialIndexing.PartialIndexing; 


public class tester {
	public static void main(String[] args) {
		SortedMap<Integer,Long> results = new TreeMap<Integer,Long>();
		String[] arg = new String[2];
		arg[0] = "InitialTest\\MedicalCollection\\00";
		for(int i=100; i<101; i+=50) {
			arg[1] = ""+i;
			results.put(i , PartialIndexing.main(arg));
		}
		File rslt = new File("results.txt");
		FileWriter fw;
		BufferedWriter br;
		try {
			fw = new FileWriter(rslt);
			br = new BufferedWriter(fw);
			br.write(results.toString());
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
