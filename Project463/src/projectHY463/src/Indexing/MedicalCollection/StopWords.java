package Indexing.MedicalCollection;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Scanner;

public class StopWords {
	public static HashMap<String, String> makeStopWordList(){
		HashMap<String, String> stopwords = new HashMap<String, String>();
		Scanner sc = null;
		try {
			sc = new Scanner(new File("5_Resources_Stoplists\\stopwordsEn.txt"));
		}catch(FileNotFoundException e) {
			System.out.println("ERROR "+e.toString());
		}
		String line;
		byte ptext[];
		try {
			while(sc.hasNext()){
				line = sc.nextLine();
				ptext = line.getBytes();
				line = new String(ptext, "UTF-8");
				stopwords.put(line, "1");
			}
		}catch(UnsupportedEncodingException e) {
			System.out.println("ERROR "+e.toString());
		}
		try {
			sc = new Scanner(new File("5_Resources_Stoplists\\stopwordsGr.txt"));
		}catch(FileNotFoundException e) {
			System.out.println("ERROR "+e.toString());
		}
		try {
			while(sc.hasNext()){
				line = sc.nextLine();
				ptext = line.getBytes();
				line = new String(ptext, "UTF-8");
				stopwords.put(line, "1");
			}
		}catch(UnsupportedEncodingException e) {
			System.out.println("ERROR "+e.toString());
		}
	    if(sc != null) {
	        sc.close();
	    }
		return stopwords;
	}
}
