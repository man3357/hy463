package Indexing.MedicalCollection;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.TreeMap;
import org.json.simple.JSONObject;
import gr.uoc.csd.hy463.NXMLFileReader;
import mitos.stemmer.Stemmer;

public class myFile{
	private int fileTotalWords;
	private int fileDiferendWords;
	private int PMCID;
	private String filePath;
	private double norm;
	private int maxFreq;
	
	private HashMap<String, Integer> titlewords;
	private HashMap<String, Integer> abstrwords;
	private HashMap<String, Integer> bodywords;
	private HashMap<String, Integer> journalwords;
	private HashMap<String, Integer> publisherwords;
	private HashMap<String, Integer> authorswords;
	private HashMap<String, Integer> categorieswords;
	
	private TreeMap<String, Integer> fileVocabulary;
	
	private static HashMap<String, String> stopwords = StopWords.makeStopWordList();
	private static int differentWords = 0;
	private static int totalWords = 0;

	
	myFile(String filePath) throws UnsupportedEncodingException, IOException{
		this.fileTotalWords=0;
		this.fileDiferendWords=0;
		this.maxFreq = 0;
		this.fileVocabulary = new TreeMap<String, Integer>(new MyComparatorString());
		this.filePath = filePath;
		this.ReadFile(this.filePath);
	}

	public int getPmcid() {return this.PMCID;}
	public int getFileTotalWords() {return fileTotalWords;}
	public int getFileDifferentWords() {return fileDiferendWords;}
	public double getNorm() {return this.norm;}
	public HashMap<String, Integer> getTitlewords(){return titlewords;}
	public HashMap<String, Integer> getAbstrwords(){return abstrwords;}
	public HashMap<String, Integer> getBodywords(){return bodywords;}
	public HashMap<String, Integer> getJournalwords(){return journalwords;}
	public HashMap<String, Integer> getPublisherwords(){return publisherwords;}
	public HashMap<String, Integer> getAuthorswords(){return authorswords;}
	public HashMap<String, Integer> getCategorieswords(){return categorieswords;}
	public TreeMap<String, Integer> getFileVocabulary(){return fileVocabulary;}
	public static int getTotalWords() {return totalWords;}
	public static int getDifferentWords() {return differentWords;}

	private static double logb( double a, double b ){return Math.log(a) / Math.log(b);}
	private static double log2( double a ){return logb(a,2);}
	
	public double tf(String s) {
		double mul = 1;
		ArrayList<String> labels = MedicalCollection.getWordFromVocabulary(s).getLabelInDocs().get(this.PMCID);
		if(labels.contains("title")) {
			mul = 3.5;
		}else if(labels.contains("authors")) {
			mul = 3;
		}else if(labels.contains("categories")) {
			mul = 2.6;
		}else if(labels.contains("abstr")) {
			mul = 2.2;
		}else if(labels.contains("journal")) {
			mul = 1.8;
		}else if(labels.contains("publisher")) {
			mul = 1.4;
		}
		return mul*((double)(this.fileVocabulary.get(s))/this.maxFreq);
	}
	
	public double calculateNorm() {
		double base;
		double sum = 0;
		double idf, tf;
		for(String s : this.fileVocabulary.keySet()) {
			tf = tf(s);
			idf = log2(MedicalCollection.files.size() / (double)MedicalCollection.getWordFromVocabulary(s).getFiles().size());
			base = tf*idf;
			sum += Math.pow(base, 2);
		}
		this.norm = Math.sqrt(sum);
		return this.norm;
	}
	
	private HashMap<String, Integer> makeHash(String txt, String label) throws FileNotFoundException, UnsupportedEncodingException {
		String delimiter = "\b\t\n\r\f\0.,;:'!?[]{}()/\\\'\"*&^%$#@_-+=~<>| ";
		StringTokenizer tokenizer = new StringTokenizer(txt, delimiter);
		HashMap<String, Integer> labelWords = new HashMap<String, Integer>();
		while(tokenizer.hasMoreTokens() ) {
			String currentToken = tokenizer.nextToken();
			if(currentToken.equals(null)) continue;
			currentToken = currentToken.toLowerCase();
			
			if( "1".equals(stopwords.get(currentToken))) {
				
			}else {
				currentToken = currentToken.replace("-", "");
				Stemmer.Initialize();
				currentToken = Stemmer.Stem(currentToken);
				if(labelWords.get(currentToken) == null){
					labelWords.put(currentToken,1);
					if(this.fileVocabulary.get(currentToken) == null){
						this.fileDiferendWords++;
						this.fileVocabulary.put(currentToken,1);
						if(MedicalCollection.vocabulary.get(currentToken) == null){
							MedicalCollection.addToVocabulary(currentToken);
							MedicalCollection.getWordFromVocabulary(currentToken).addFile(this,fileTotalWords,label);
							differentWords++;
						}else {
							MedicalCollection.getWordFromVocabulary(currentToken).addFile(this,fileTotalWords,label);
						}
					}else {
						this.fileVocabulary.put(currentToken,this.fileVocabulary.get(currentToken)+1);
						MedicalCollection.getWordFromVocabulary(currentToken).addFile(this,fileTotalWords,label);
					}
					if(this.maxFreq < this.fileVocabulary.get(currentToken)) {
						this.maxFreq = this.fileVocabulary.get(currentToken);
					}
				}else{
					labelWords.put(currentToken,labelWords.get(currentToken)+1);
				}
			}
			this.fileTotalWords++;
			totalWords++;
		}
		return labelWords;
	}
	
	public void ReadFile(String filepath) throws IOException {
		File example = new File(filepath);
		NXMLFileReader xmlFile = new NXMLFileReader(example);
		String pmcid = xmlFile.getPMCID();
		this.PMCID = Integer.parseInt((pmcid));
		byte ptext[] = pmcid.getBytes();
		pmcid = new String(ptext, "UTF-8");
		String title = xmlFile.getTitle();
		ptext = title.getBytes();
		title = new String(ptext, "UTF-8");
		String abstr = xmlFile.getAbstr();
		ptext = abstr.getBytes();
		abstr = new String(ptext, "UTF-8");
		String body = xmlFile.getBody();
		ptext = body.getBytes();
		body = new String(ptext, "UTF-8");
		String journal = xmlFile.getJournal();
		ptext = journal.getBytes();
		journal = new String(ptext, "UTF-8");
		String publisher = xmlFile.getPublisher();
		ptext = publisher.getBytes();
		publisher = new String(ptext, "UTF-8");
		ArrayList<String> authors0 = xmlFile.getAuthors();
		String authors = new String();
		for(String s: authors0) {
			ptext = s.getBytes();
			authors = authors + (new String(ptext, "UTF-8"));
		}
		HashSet<String> categories0 = xmlFile.getCategories();
		String categories = new String();
		for(String s: categories0) {
			ptext = s.getBytes();
			categories = categories + (new String(ptext, "UTF-8"));
		}
		
		this.titlewords = makeHash(title,"title");
		this.abstrwords = makeHash(abstr,"abstr");
		this.bodywords = makeHash(body,"body");
		this.journalwords = makeHash(journal,"journal");
		this.publisherwords = makeHash(publisher,"publisher");
		this.authorswords = makeHash(authors,"authors");
		this.categorieswords = makeHash(categories,"categories");
		
	}
	
	public String toString() {
		String toRet = "{";
		toRet += this.PMCID + "\n";
		toRet += this.filePath + "\n";
		toRet += this.norm  + "}\n";
		return toRet;
	}
	public byte[] fileInfo() {
		String tf = "";
		JSONObject json = new JSONObject();
		for(String s : this.fileVocabulary.keySet()) {
			tf += tf(s)+"|";
		}
		tf = tf.substring(0, tf.length()-1);
		json.put("filepath", this.filePath);
		json.put("pmcid", this.PMCID);
		json.put("tf", tf);
		String toRet = json + "\n";
//		toRet += this.PMCID +"\\";
//		toRet += this.norm  + "}\n";
		return toRet.getBytes();
	}

	public static void clear() {
		totalWords = 0;
		differentWords = 0;
	}
}
