package Indexing.MedicalCollection;



import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.TreeMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;


public class MedicalCollection {
	protected static TreeMap<String, myWord> vocabulary = new TreeMap<String, myWord>(new MyComparatorString());
	protected static TreeMap<String, myFile> files = new TreeMap<String, myFile>(new MyComparatorInt());
	protected static TreeMap<String, Long> documentsPosInDocuments = new TreeMap<String, Long>();
	protected static TreeMap<String, Long> wordsPosInPosting = new TreeMap<String, Long>();
	protected static TreeMap<String, Long> wordsPosInVocabulary = new TreeMap<String, Long>();
	protected static int fileNo;
	
	protected static void addToVocabulary(String word) {
		vocabulary.put(word, new myWord(word));
	}
	
	protected static myWord getWordFromVocabulary(String word) {
		return vocabulary.get(word);
	}
	
	public static <T> void printToFile(TreeMap<String, T> m, String filePath) throws IOException {
		FileWriter fstream = new FileWriter(filePath,true);
		BufferedWriter out = new BufferedWriter(fstream);
		for(String s : m.keySet()) {
			out.append(s+"|");
			out.append(m.get(s).toString());
			out.append("\n");
		}
		if(out != null) {
			out.close();
		}
	}
	
	public static void listFilesForFolder(File folder) throws UnsupportedEncodingException, IOException, ParseException {
		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				System.out.println("LATHOS");
				listFilesForFolder(fileEntry);
			} else {
				myFile file = new myFile(fileEntry.getAbsolutePath());
				files.put(file.getPmcid()+"", file);
			}
		}

		RandomAccessFile file = new RandomAccessFile("CollectionIndex\\toMerge\\Documents\\Documents\\documents"+fileNo+".txt","rw");
		RandomAccessFile file0 = null;
		for(String s : files.keySet()) {
			file0 = new RandomAccessFile("CollectionIndex\\toMerge\\Documents\\Words\\"+files.get(s).getPmcid()+".txt" , "rw");
			for(String w : files.get(s).getFileVocabulary().keySet()) {
				JSONObject json = new JSONObject();
				json.put("word", w);
				json.put("tf", files.get(s).tf(w));
				json.put("fileNo",fileNo);
				file0.write((json+"\n").getBytes());
			}
			documentsPosInDocuments.put(s, file.getFilePointer());
			file.write(files.get(s).fileInfo());
		}
		file.close();
		file0.close();
		file = new RandomAccessFile("CollectionIndex\\toMerge\\Postings\\posting"+fileNo+".txt","rw");
		file0 = new RandomAccessFile("CollectionIndex\\toMerge\\Vocabularies\\vocabulary"+fileNo+".txt","rw");
		for(String s : vocabulary.keySet()) {
			wordsPosInPosting.put(s, file.getFilePointer());
			file.write(vocabulary.get(s).filesAppears());
			wordsPosInVocabulary.put(s, file.getFilePointer());
			file0.write(vocabulary.get(s).wordInfo());
		}
		file.close();
		file0.close();
	}

	public static void run(String string, int num) {
		fileNo = num;
		File folder = new File(string);
		try {
			listFilesForFolder(folder);
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		vocabulary.clear();
		files.clear();
		documentsPosInDocuments.clear();
		wordsPosInPosting.clear();
		wordsPosInVocabulary.clear();
		myFile.clear();
		myWord.clear();
	}
	
	public static void main(String[] args) throws IOException, ParseException {
		run(args[0] , 0);
	}
}
