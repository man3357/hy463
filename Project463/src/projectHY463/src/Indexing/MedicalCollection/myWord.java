package Indexing.MedicalCollection;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONObject;

public class myWord {

	private String name;
	private ArrayList<myFile> files;
	private long posInPosting;
	private HashMap<Integer, ArrayList<Long>> posInDocs;
	private HashMap<Integer, ArrayList<String>> labelInDocs;
	
	myWord(String name){
		this.name = name;
		this.files = new ArrayList<myFile>();
		posInDocs = new HashMap<Integer, ArrayList<Long>>();
		labelInDocs = new HashMap<Integer, ArrayList<String>>();
	}
	
	myWord(String name, long pos){
		this.name = name;
		this.posInPosting = pos;
		this.files = new ArrayList<myFile>();
		posInDocs = new HashMap<Integer, ArrayList<Long>>();
		labelInDocs = new HashMap<Integer, ArrayList<String>>();
	}
	
	public HashMap<Integer, ArrayList<String>> getLabelInDocs(){return this.labelInDocs;}
	
	ArrayList<myFile> getFiles(){return this.files;}
	String getName() {return this.name;}
	
	void addFile(myFile file, long pos, String label) {
		if(!this.files.contains(file)) {
			this.files.add(file);
			this.posInDocs.put(file.getPmcid(), new ArrayList<Long>());
			this.labelInDocs.put(file.getPmcid(), new ArrayList<String>());
		}
		this.posInDocs.get(file.getPmcid()).add(pos);
		this.labelInDocs.get(file.getPmcid()).add(label);
	}
	
	public int df() {return this.files.size();}
	
	public String getPosInFile(myFile file) {
		String toRet = "";
		String label = "";
		for(int i=0 ; i<this.labelInDocs.get(file.getPmcid()).size() ; i++) {
			label += this.labelInDocs.get(file.getPmcid()).get(i)+",";
		}
		label = label.substring(0, label.length()-1);
		toRet +="["+label+"]";
		return toRet;
	}
	
//	public String getLabelInFile(myFile file) {
//		String toRet = "[";
//		for(String s : this.labelInDocs.get(file.getPmcid())) {
//			toRet += s+",";
//		}
//		toRet = toRet.substring(0, toRet.length()-1);
//		toRet += "]";
//		return toRet;
//	}
	
	public byte[] filesAppears() {
		JSONObject json = new JSONObject();
		String toRet = "";
//		toRet = "{"+this.name+":\n";
		for( myFile f : this.files) {
			json.put("pmcid", f.getPmcid());
			json.put("tf", new DecimalFormat("##.##").format(f.tf(this.name)));
			String pl = getPosInFile(f);
			json.put("label", pl);
			json.put("doc", MedicalCollection.documentsPosInDocuments.get(f.getPmcid()+""));
			toRet += json;
		}
		json = new JSONObject();
		json.put("word", this.name);
		json.put("files", toRet);
		toRet = json + "\n";
		
		return toRet.getBytes();
	}
	
	public byte[] wordInfo() {
		JSONObject json = new JSONObject();
		json.put("fileNo", MedicalCollection.fileNo);
		json.put("word", this.name);
		json.put("df", this.df());
		json.put("posInPosting", MedicalCollection.wordsPosInPosting.get(this.name));
		String toRet = json + "\n";
		return toRet.getBytes();
	}
	
	public String toString() {
		String toRet = "\n";
		toRet += name.toUpperCase() +" ";
//		for( myFile f : this.files) {
//			toRet += f.getPmcid() + ", ";
//		}
//		toRet = toRet.substring(0, toRet.length()-2);
		toRet += this.files.size() + " files";
		return toRet;
	}

	public static void clear() {
		
	}
}
