package Indexing.PartialIndexing;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import org.json.simple.parser.ParseException;

import Indexing.MedicalCollection.MedicalCollection;
import Indexing.merge.Merge;


public class PartialIndexing {
	public static int TOTALFILES = 0;
	private static int MAXFILES;
	private static int fn = 0;
	private static int c;

	public static void listFilesForFolder(File folder){
		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFilesForFolder(fileEntry);
			} else {
				try {
				if(c == MAXFILES) {
					c=0;
					if (!(new File("InitialTest\\MyMedicalCollection\\"+(++fn)).mkdirs())) {
						System.out.println("FAILED to create new folder. InitialTest\\MyMedicalCollection\\"+(fn));
					}
				}
					File newFile = new File("InitialTest\\MyMedicalCollection\\"+fn+"\\"+fileEntry.getName());
					Files.copy(fileEntry.toPath(), newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
					//Files.move(fileEntry.toPath(), newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
					TOTALFILES++;
					c++;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private static String timer(long startTime) {
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		double sec = totalTime/1000;
		int min = (int)sec/60;
		sec = sec%60;
		return min+"minutes and "+(int)sec+"seconds.";
	}
	
	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		File folder = new File(args[0]);
		MAXFILES = Integer.parseInt(args[1]);
		c = MAXFILES;
		dileteFolder(new File("CollectionIndex"));
		dileteFolder(new File("InitialTest\\MyMedicalCollection"));
		listFilesForFolder(folder);
		try {
			if (!(new File("CollectionIndex\\toMerge").mkdirs())) {
				System.out.println("CollectionIndex\\toMerge----------------");
			}
			if (!(new File("CollectionIndex\\toMerge\\Documents\\Documents").mkdirs())) {
				System.out.println("CollectionIndex\\toMerge\\Documents\\Documents");
			}
			if (!(new File("CollectionIndex\\toMerge\\Documents\\Words").mkdirs())) {
				System.out.println("CollectionIndex\\toMerge\\Documents\\Words");
			}
			if (!(new File("CollectionIndex\\toMerge\\Postings").mkdirs())) {
				System.out.println("CollectionIndex\\toMerge\\Postings");
			}
			if (!(new File("CollectionIndex\\toMerge\\Vocabularies").mkdirs())) {
				System.out.println("CollectionIndex\\toMerge\\Vocabularies");
			}
			File myFolder = new File("InitialTest\\MyMedicalCollection");
			for(int i=0; i<myFolder.listFiles().length; i++) {
				MedicalCollection.run(myFolder.listFiles()[i].getAbsolutePath(),i+1);
			}
			System.out.println("Partial Indexing DONE!");
			System.out.println("Time for partial indexing: "+timer(startTime));
			Merge.run("CollectionIndex\\toMerge");
			System.out.println("Merge DONE!");
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		System.out.println("Total time: "+timer(startTime));
		//return (System.currentTimeMillis() - startTime) / 1000;
	}
	
	private static void dileteFolder(File f) {
		if(!f.exists()) return;
		for (File fileEntry : f.listFiles()) {
			if (fileEntry.isDirectory()) {
				dileteFolder(fileEntry);
			} else {
				fileEntry.delete();
			}
		}
		f.delete();
	}
}
