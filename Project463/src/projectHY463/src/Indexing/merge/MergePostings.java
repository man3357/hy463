package Indexing.merge;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MergePostings {

	private static void writetoFile(String word, ArrayList<JSONObject> docs, RandomAccessFile posting) {
		String toWrite = "{"+word+":\n";
		try {
			Merge.wordsPosInPosting.put(word, posting.getFilePointer());
			for(JSONObject j : docs){
				j.remove("doc");
				j.put("doc",Merge.documentsPosInDocuments.get(j.get("pmcid").toString()));
				toWrite += "\t";
				toWrite += j.toJSONString();
				toWrite += "\n";
			}
			toWrite += "}\n";
			posting.write(toWrite.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static ArrayList<JSONObject> mergeWord(ArrayList<ArrayList<JSONObject>> toWrite) {
		ArrayList<JSONObject> toRet = new ArrayList<JSONObject>();
		for(ArrayList<JSONObject> j : toWrite) {
			toRet.addAll(j);
		}
		return toRet;
	}
	
	private static String compareAll(ArrayList<String> words) {
		if(words.size() == 0) return null;
		ArrayList<String> tmp = new ArrayList<String>(words);
		Collections.sort(tmp);
		return tmp.get(0);
	}

	private static ArrayList<JSONObject> parseLine(String line) throws ParseException{
		JSONParser strParser = new JSONParser();
		JSONObject json;
		ArrayList<JSONObject> jsonList = new ArrayList<JSONObject>();
		for( String s : line.split("}")) {
			json = (JSONObject) strParser.parse(s+"}");
			jsonList.add(json);
		}
		return jsonList;
	}
	
	private static void mergePostings(ArrayList<File> postings) throws FileNotFoundException, ParseException {
		ArrayList<ArrayList<JSONObject>> josnLists = new ArrayList<ArrayList<JSONObject>>();
		ArrayList<String> words = new ArrayList<String>();
		HashMap<ArrayList<JSONObject>,File> file = new HashMap<ArrayList<JSONObject>,File>();
		HashMap<File,Scanner> sc = new HashMap<File,Scanner>();
		JSONParser strParser = new JSONParser();
		RandomAccessFile Posting = new RandomAccessFile("CollectionIndex\\Final\\Posting.txt","rw");
		for(File f : postings) {
			sc.put(f,new Scanner(f));
			String line = sc.get(f).nextLine().toString();
			JSONObject json = (JSONObject) strParser.parse(line);
			ArrayList<JSONObject> files = parseLine(json.get("files").toString());
			words.add(json.get("word").toString());
			josnLists.add(files);
			file.put(files, f);
		}
		String equals;
		while(true) {
			ArrayList<JSONObject> word = new ArrayList<JSONObject>();
			equals = compareAll(words);
			if(equals != null) {
				ArrayList<ArrayList<JSONObject>> toWrite = new ArrayList<ArrayList<JSONObject>>();
				for(int i=0; i<words.size(); i++) {
					if(equals.equals(words.get(i))) {
						ArrayList<JSONObject> files = josnLists.remove(i);
						words.remove(i);
						File f = file.remove(files);
						toWrite.add(files);
						i--;
						if( sc.get(f).hasNextLine() ){
							String line = sc.get(f).nextLine();
							JSONObject json = (JSONObject) strParser.parse(line);
							files = parseLine(json.get("files").toString());
							words.add(json.get("word").toString());
							josnLists.add(files);
							file.put(files, f);
						}else {
							sc.remove(f).close();
						}
					}
				}
				word = mergeWord(toWrite);
			}else {
				break;
			}
			writetoFile(equals,word,Posting);
		}
		try {
			Posting.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void run(String filepath) throws IOException, ParseException {
		ArrayList<File> postings = new ArrayList<File>();
		for (File ps : (new File(filepath)).listFiles()) {
			postings.add(ps);
		}
		mergePostings(postings);
	}
}
