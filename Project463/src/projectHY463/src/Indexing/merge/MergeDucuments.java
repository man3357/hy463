package Indexing.merge;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MergeDucuments {

	public static void run(String filepath) throws ParseException, IOException {
		ArrayList<File> documents = new ArrayList<File>();
		for (File f : (new File(filepath)).listFiles()) {
			documents.add(f);
		}
		mergeDocuments(documents);		
	}

	private static double logb( double a, double b ){return Math.log(a) / Math.log(b);}
	private static double log2( double a ){return logb(a,2);}
	
	private static TreeMap<String,Double> calculateW(JSONObject doc) throws ParseException, IOException {
		JSONParser strParser = new JSONParser();
		TreeMap<String,Double> wij = new TreeMap<String,Double>();
		int numOfDocs = Indexing.PartialIndexing.PartialIndexing.TOTALFILES;
		File file = new File("CollectionIndex\\toMerge\\Documents\\Words\\"+doc.get("pmcid")+".txt");
		Scanner sc = new Scanner(file);
		double w;
		double sum = 0;
		double idf;
		while(sc.hasNextLine()) {
			String line = sc.nextLine();
			JSONObject json = (JSONObject) strParser.parse(line);
			String word = json.get("word").toString();
			idf = log2( numOfDocs / Double.parseDouble(Merge.dfs.get(word).toString()));
			w =  Double.parseDouble(json.get("tf").toString()) * idf;
			wij.put(word,w);
			sum += Math.pow(w, 2);
		}
		wij.put("_norm", Math.sqrt(sum));
		sc.close();
		return wij;
	}
	
	private static void mergeDocuments(ArrayList<File> documents) throws ParseException, IOException {
		JSONParser strParser = new JSONParser();
		ArrayList<JSONObject> lines = new ArrayList<JSONObject>();
		HashMap <JSONObject,File> files = new HashMap<JSONObject,File>();
		HashMap <File,Scanner> scanner= new HashMap <File,Scanner>();
		for(File d: documents) {
			Scanner sc = new Scanner(d);
			scanner.put(d, sc);
			String line=sc.nextLine().toString();
			JSONObject json=(JSONObject) strParser.parse(line);
			files.put(json, d);
			lines.add(json);
		}
		RandomAccessFile Documents= new RandomAccessFile("CollectionIndex\\Final\\Documents.txt", "rw");
		while(true) {
			if(lines.size()==0) {
				break;
			}
			String pmcid= comparelines(lines);
			JSONObject line= findline(lines,pmcid);
			lines.remove(line);
			File f=files.remove(line);
			Merge.documentsPosInDocuments.put(pmcid, Documents.getFilePointer());
			TreeMap<String,Double> wij = calculateW(line);
			line.remove("tf");
			line.put("norm", wij.remove("_norm"));
			line.put("wij",wij.toString());
			line.put("totalFiles", Indexing.PartialIndexing.PartialIndexing.TOTALFILES);
			String toWrite =line.toString()+"\n";
			Documents.write(toWrite.getBytes());
			if(scanner.get(f).hasNextLine()) {
				String line1=scanner.get(f).nextLine();
				JSONObject json=(JSONObject) strParser.parse(line1);
				files.put(json,f);
				lines.add(json);
			}else {
				scanner.get(f).close();
			}
		}
		Documents.close();
	}

	private static JSONObject findline(ArrayList<JSONObject> lines, String pmcid) {
		for(JSONObject l:lines) {
			if(pmcid.equals(l.get("pmcid").toString())){
				return l;
			}
		}
		return null;
	}

	private static String comparelines(ArrayList<JSONObject> lines) {
		int min=Integer.parseInt(lines.get(0).get("pmcid").toString());
		for(JSONObject l: lines) {
			if(min>Integer.parseInt(l.get("pmcid").toString())) {
				min=Integer.parseInt(l.get("pmcid").toString());
			}
		}
		return min+"";
	}
}
