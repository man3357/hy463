package Indexing.merge;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.TreeMap;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class Merge {
	protected static TreeMap<String, Long> documentsPosInDocuments = new TreeMap<String, Long>();
	protected static TreeMap<String, Long> wordsPosInPosting = new TreeMap<String, Long>();
	protected static TreeMap<String, Long> wordsPosInVocabulary = new TreeMap<String, Long>();
	protected static TreeMap<String, Integer> dfs = new TreeMap<String, Integer>(new MyComparatorString());
	public static int N;
	private static void writetoFile(String word, int df, RandomAccessFile vocabulary) {
		JSONObject json = new JSONObject();
		try {
			Merge.wordsPosInVocabulary.put(word, vocabulary.getFilePointer());
			json.put("word", word);
			json.put("df", df);
			json.put("posInPosting", Merge.wordsPosInPosting.get(word));
			String toWrite = json + "\n";
			vocabulary.write(toWrite.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeVoc() throws IOException {
		RandomAccessFile vocabulary = new RandomAccessFile("CollectionIndex\\Final\\Vocabulary.txt","rw");
		for(String w : dfs.keySet()) {
			writetoFile(w,dfs.get(w),vocabulary);
		}
		vocabulary.close();
	}
	
	public static void run(String string) throws IOException, ParseException {
		if (!(new File("CollectionIndex\\Final").mkdirs())) {
			System.out.println("CollectionIndex\\Final");
		}
		MergeVocabularies.run(string + "\\Vocabularies");
		System.out.println("Merge Vocabularies DONE!");
		MergeDucuments.run(string + "\\Documents\\Documents");
		System.out.println("Merge Documents DONE!");
		MergePostings.run(string + "\\Postings");
		System.out.println("Merge Postings DONE!");
		N = documentsPosInDocuments.size();
		writeVoc();
	}
}
