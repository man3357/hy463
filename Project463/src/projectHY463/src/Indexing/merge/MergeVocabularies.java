package Indexing.merge;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class MergeVocabularies {
	
	private static String compareAll(ArrayList<String> words) {
		if(words.size() == 0) return null;
		ArrayList<String> tmp = new ArrayList<String>(words);
		Collections.sort(tmp);
		return tmp.get(0);
	}

	private static void mergeVocabularies(ArrayList<File> vocabularies) throws FileNotFoundException, ParseException {
		ArrayList<String> words = new ArrayList<String>();
		ArrayList<JSONObject> josnLists = new ArrayList<JSONObject>();
		HashMap<JSONObject,File> file = new HashMap<JSONObject,File>();
		HashMap<File,Scanner> sc = new HashMap<File,Scanner>();
		JSONParser strParser = new JSONParser();
		for(File f : vocabularies) {
			sc.put(f,new Scanner(f));
			String line = sc.get(f).nextLine().toString();
			JSONObject json = (JSONObject) strParser.parse(line);
			words.add(json.get("word").toString());
			josnLists.add(json);
			file.put(json,f);
		}
		while(true) {
			String less = compareAll(words);
			int df = 0;
			if(less != null) {
				for(int i=0; i<words.size(); i++) {
					if(less.equals(words.get(i))) {
						df += Integer.parseInt(josnLists.get(i).get("df").toString());
						words.remove(i);
						JSONObject j = josnLists.remove(i--);
						File f = file.remove(j);
						if( sc.get(f).hasNextLine() ){
							String line = sc.get(f).nextLine().toString();
							JSONObject json = (JSONObject) strParser.parse(line);
							words.add(json.get("word").toString());
							josnLists.add(json);
							file.put(json, f);
						}else {
							sc.remove(f).close();
						}
					}
				}
			}else {
				break;
			}
			Merge.dfs.put(less, df);
			//writetoFile(less,df,vocabulary);
		}
	}

	public static void run(String filepath) throws IOException, ParseException {
		ArrayList<File> vocabularies = new ArrayList<File>();
		for (File voc : (new File(filepath)).listFiles()) {
			vocabularies.add(voc);
		}
		mergeVocabularies(vocabularies);
	}

}
