package Indexing.merge;

import java.util.Comparator;

class MyComparatorString implements Comparator<String>{
    public int compare(String o1,String o2)
    {
        return o1.compareTo(o2);
    }
}
class MyComparatorInt implements Comparator<String>{
	public int compare(String o1, String o2) {
		int i1 = Integer.parseInt(o1);
		int i2 = Integer.parseInt(o2);
		return i1-i2;
	}
}
